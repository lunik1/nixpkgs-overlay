{
  description = "My (lunik1) dumb nixpkgs overlay";

  inputs = { nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable"; };

  outputs = { self, nixpkgs, ... }: rec {
    overlay = self: super: rec {
      myosevka = super.iosevka.override {
        privateBuildPlan = import iosevka/myosevka.nix;
        set = "myosevka";
      };
      myosevka-proportional = super.iosevka.override {
        privateBuildPlan = import iosevka/myosevka-proportional.nix;
        set = "myosevka-proportional";
      };
      myosevka-aile = super.iosevka.override {
        privateBuildPlan =
          (import iosevka/myosevka-aile.nix) { lib = super.lib; };
        set = "myosevka-aile";
      };
      myosevka-etoile = super.iosevka.override {
        privateBuildPlan =
          (import iosevka/myosevka-etoile.nix) { lib = super.lib; };
        set = "myosevka-etoile";
      };
    };
  };
}
