{
  family = "Myosevka";
  spacing = "fixed";
  serifs = "sans";
  no-cv-ss = true;
  no-ligation = true;
  variants = {
    design = {
      ampersand = "upper-open";
      capital-g = "toothless-corner-serifless-hooked";
      caret = "high";
      eight = "two-circles";
      eszet = "longs-s-lig";
      four = "closed";
      l = "tailed-serifed";
      nine = "closed-contour";
      number-sign = "upright-open";
      one = "base";
      paragraph-sign = "low";
      seven = "curly-serifless";
      six = "closed-contour";
    };
    italic.eszet = "longs-s-lig-tailed";
  };
  widths.normal = {
    shape = 576;
    menu = 5;
    css = "normal";
  };
}
